#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 10 10:00:43 2019

@author: sohel
"""

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.cross_validation import train_test_split

class scaler(object):
    
    def __init__(self):
        pass
    
    def scalling(self,x):
        sc=MinMaxScaler(feature_range=(0,1))
        scaled_data=sc.fit_transform(x)
        return(scaled_data)
        
        
    def __del__(self):
        pass
   
class splitter(object):
    
    def __init__(self):
        pass
    
    def decomposition(self,x,y,test_size=0.25,random_state=0):
        x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=test_size,random_state=random_state)
        return(x_train,x_test,y_train,y_test)
        
    def __del__(self):
        pass


class Encoder(object):
    def __init__(self):
        pass
    
    def getdummy(self,x):
        dummy=pd.get_dummies(x)
        return(dummy)
        
    def __del__(self):
        pass
        
    





















